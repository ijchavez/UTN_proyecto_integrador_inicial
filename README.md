# UTN Proyecto Integrador Inicial
# ABMC de Libros para Librería/Biblioteca

Esta aplicación es una herramienta para gestionar libros en una librería o biblioteca. Permite realizar Altas, Bajas, Modificaciones y Consultas (ABMC) de libros. La aplicación está desarrollada en Python utilizando Tkinter para la interfaz gráfica y SQLite3 como base de datos.

## Funcionalidades

- **Alta de libros**: Permite agregar nuevos libros a la base de datos.
- **Consulta de libros**: Permite buscar libros por título o ISBN.
    - **Modificar libro**: Una vez encontrado un libro, se puede modificar su información.
    - **Eliminar libro**: Una vez encontrado un libro, se puede eliminar de la base de datos.
- **Reportes**: Permite obtener reporte de movimientos mensuales o de libros por género.

## Tecnologías Utilizadas

- **Python**: Lenguaje de programación principal.
- **Tkinter**: Biblioteca para crear la interfaz gráfica de usuario.
- **SQLite3**: Motor de base de datos para almacenar la información de los libros.

## Estructura del Proyecto

```
biblioteca/
│
├── src/
│ ├── constructor/
│ │ ├── book.py
│ │ └── category.py
│ ├── crud/
│ │ ├── add.py
│ │ └── search.py
│ ├── db/
│ │ └── biblioteca.db
│ ├── utilities/
│ │ ├── helpers.py
│ │ ├── db_utils.py
│ │ └── ui_utils.db
│ └── main_window.py
│
└── README.md
```

## Instalación y Ejecución

1. **Clonar el repositorio**:
    ```sh
    git https://gitlab.com/ijchavez/UTN_proyecto_integrador_inicial
    cd UTN_proyecto_integrador_inicial

    ```

2. **Instalar dependencias**:
   Asegúrate de tener Python instalado en tu sistema. Las dependencias necesarias son `tkinter` y `sqlite3`, que generalmente vienen preinstaladas con Python.

3. **Ejecutar la aplicación**:
    ```sh
    python biblioteca/src/main_window.py
    ```

## Uso de la Aplicación

1. **Pantalla Inicial**: Al iniciar la aplicación, se muestran tres pestañas: "Alta", "Consulta" y "Reportes".
    - **Alta**: Abre el formulario para agregar un nuevo libro.
    - **Consulta**: Abre el formulario para buscar libros en la base de datos.

2. **Alta de Libros**:
    - Completa los campos ISBN, Título, Autor y Género.
    - Presiona el botón "Agregar Libro" para almacenar la información en la base de datos.
    - Los campos se blanquearán automáticamente después de agregar un libro correctamente.

### Validaciones
   -  Todos los campos son obligatorios
   -  El ISBN tiene que tener la siguiente estructura xxx-xx-x-xxxxx-x y no debe existir en base de datos

3. **Consulta de Libros**:
    - Busca libros por Título, ISBN o Género.
    - Si se encuentran libros, se mostrarán en una tabla con botones para "Modificar" y "Eliminar".
    - **Modificar**: Permite actualizar la información del libro seleccionado.
    - **Eliminar**: Permite borrar el libro seleccionado de la base de datos.

### Validaciones
   -  Para modificar son las mismas que en el alta.
   -  Para borrar el libro no tiene que tener transacciones de ingreso o egreso.

4. **Reportes**:
   - Reporte de libro por Género.
   - Reporte de movimientos mensuales

## Contribuciones

Si deseas contribuir a este proyecto, por favor abre un issue o envía un pull request con tus sugerencias o mejoras.
