import tkinter as tk
from tkinter import messagebox
import re

from biblioteca.src.constructor.book import Book


def validate_add_book(title, author, isbn, genre):
    """
    Validates the book fields before adding it to the database.

    Checks that all required fields (Title, Author, ISBN, Genre) are filled and that the ISBN has a valid format.

    Args:
        title (str): The title of the book.
        author (str): The author of the book.
        isbn (str): The ISBN of the book.
        genre (str): The genre of the book.

    Returns:
        bool: True if all validations pass, False otherwise.

    Side Effects:
        Displays a message box with an error message if any validation fails.
        Displays a message box with a success message if all validations pass.

    Example:
        >>> validate_add_book("The Great Gatsby", "F. Scott Fitzgerald", "978-0-74-32735-5", "Romance")
        True
    """
    if not (title and author and isbn and genre):
        messagebox.showerror("Error", "Los campos Título, Autor, ISBN y Género son obligatorios.")
        return False
    if isbn:
        if not validate_isbn_regex(isbn):
            return False

    messagebox.showinfo("Éxito", "Libro agregado correctamente")
    return True


def validate_search(self, title, isbn, genre):
    """
    Validates the search input for book title and ISBN.

    Ensures that either title or ISBN is provided (not both), and that they conform to the required format.

    Args:
        self
        title (str): The title of the book to search for.
        isbn (str): The ISBN of the book to search for.
        genre (str): The Genre of the book to search for.
    Returns:
        bool: True if the search input is valid, False otherwise.

    Side Effects:
        Displays a message box with a warning if the validation fails.
        - Warns if both title and ISBN are empty.
        - Warns if both title and ISBN are provided.
        - Warns if title or ISBN do not match the required format.

    Example:
        >>> validate_search(self, "The Great Gatsby", "")
        True

        >>> validate_search(self, "", "9780743273565")
        True

        >>> validate_search(self, "", "", "Romance")
        True

        >>> validate_search(self, "The Great Gatsby", "9780743273565", "")
        None

        >>> validate_search(self, "", "", "")
        None
    """
    search_validated = True
    if not title and not isbn and not genre:
        messagebox.showwarning("Advertencia", "Por favor ingresa al menos el título, el ISBN o el genéro del libro.")
        return

    if (title and isbn) or (title and genre) or (isbn and genre):
        messagebox.showwarning(
            "Advertencia", "Por favor ingresa solo el título, el ISBN o el genéro del libro, no mas de uno.")
        return

    if title:
        if not validate_title_regex(title):
            search_validated = False

    if isbn:
        if not validate_isbn_regex(isbn):
            search_validated = False

    if genre:
        search_validated = True

    return search_validated


def validate_title_regex(title):
    """
    Validates the title regex.

    This function checks if the provided ISBN is regex valid.

    Args:
        title (tk.Entry or str): The title to be validated.

    Returns:
        bool: True if the title regex meet expectations or not, False otherwise.

    Raises:
        messagebox.showerror: Displays an error message if the title is regex invalid.
    """
    # if titulo and not titulo.isalnum(): (BUG)
    # if titulo and not re.match(r"^[a-zA-Z0-9\s]+$", titulo): (BUG) no á/ñ
    if title and not re.match(r"^[a-zA-Z0-9\sáéíóúÁÉÍÓÚüÜñÑ]+$", title):
        messagebox.showerror("Error", "El título debe contener solo letras y números.")
        return False
    return True


regex_pattern = r'^\d{3}-\d-\d{2}-\d{6}-\d$'


def validate_isbn_regex(isbn):
    """
    Validates the ISBN regex.

    This function checks if the provided ISBN is regex valid.

    Args:
        isbn (tk.Entry or str): The ISBN entry widget or a string to be validated.

    Returns:
        bool: True if the ISBN regex meet expectations or not, False otherwise.

    Raises:
        messagebox.showerror: Displays an error message if the ISBN is regex invalid.
    """
    # if isbn and not re.match(r"^\d{3}-\d{1,5}-\d{1,7}-\d{1}$", isbn): (BUG)
    if isbn and not re.match(regex_pattern, isbn):
        messagebox.showerror("Error", "El formato del ISBN es incorrecto. Debe tener el formato xxx-x-xx-xxxxxx-x.")
        return False
    return True


def validate_isbn(isbn_entry, current_book_id=None):
    """
    Validates the ISBN entry.

    This function checks if the provided ISBN is valid. It first strips any leading or trailing whitespace
    from the ISBN entry. It then checks if the ISBN follows the correct format using a regular expression.
    Additionally, it verifies if the ISBN already exists in the database, and if so, ensures it is not
    associated with a different book unless the `current_book_id` is provided.

    Args:
        isbn_entry (tk.Entry or str): The ISBN entry widget or a string to be validated.
        current_book_id (int, optional): The ID of the current book being modified. If provided, the
            function will allow the ISBN to match the current book's ISBN.

    Returns:
        bool: True if the ISBN is valid and does not exist in the database, False otherwise.

    Raises:
        messagebox.showerror: Displays an error message if the ISBN is invalid or already exists.
    """
    isbn = isbn_entry.get().strip()
    if isbn:
        if not validate_isbn_regex(isbn):
            isbn_entry.focus_set()
            return False
        book_found, book_id = Book.find_book_by_isbn(isbn)
        if book_found is not None and (current_book_id is None or book_id != current_book_id):
            messagebox.showerror(
                "Error", "El ISBN ya existe en la base de datos. Por favor, ingresa un ISBN diferente.")
            isbn_entry.focus_set()
            return False
    return True


def clear_fields(isbn_entry, title_entry, author_entry, genre_entry):
    """
    Clears entry fields.

    This function clears the fields sent by parameter. The idea is to use it once an action takes place:
        - Moving from tabs
        - Adding a book

    Args:
        isbn_entry (tk.Entry): The ISBN entry widget.
        title_entry (tk.Entry): The Title entry widget.
        author_entry (tk.Entry): The Author entry widget.
        genre_entry (tk.Entryr): The Genre entry widget.

    Returns:
        None

    """
    isbn_entry.delete(0, tk.END)
    title_entry.delete(0, tk.END)
    author_entry.delete(0, tk.END)
    genre_entry.set('')
