import tkinter as tk
from tkinter import ttk


def create_label_entry_pair(frame, label_text, row, column, entry_type='entry', entry_values=None):
    """
    Creates a label and an entry widget or combobox pair in a specified frame.

    Args:
        frame (tk.Frame): The frame where the label and entry will be placed.
        label_text (str): The text for the label.
        row (int): The row position in the grid layout for the label and entry.
        column (int): The column position in the grid layout for the label and entry.
        entry_type (str): The type of entry widget ('entry' or 'combobox'). Default is 'entry'.
        entry_values (list): The values for the combobox. Default is None.

    Returns:
        tk.Entry or ttk.Combobox: The entry or combobox widget created.
    """
    tk.Label(frame, text=label_text).grid(row=row, column=column, sticky="w")
    if entry_type == 'entry':
        entry = tk.Entry(frame)
    elif entry_type == 'combobox':
        entry = ttk.Combobox(frame, values=entry_values, state="readonly")
    else:
        raise ValueError("Invalid entry_type. Use 'entry' or 'combobox'.")
    entry.grid(row=row, column=column+1, padx=5, pady=5)
    return entry
