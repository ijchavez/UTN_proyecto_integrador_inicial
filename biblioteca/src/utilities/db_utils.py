import sqlite3
import os


def get_directory(directory):
    """
    Retrieves the absolute path of a directory relative to the current file's directory.

    Args:
        directory (str): The relative directory path.

    Returns:
        str: The absolute path of the directory.
    """
    current_directory = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(current_directory, directory)


def get_cursor():
    """
    Establishes a connection to the database and creates the necessary tables if they do not exist.

    Returns:
        tuple: A tuple containing the database connection and the cursor.
    """
    db_path = get_directory('../db/biblioteca.db')
    conn = sqlite3.connect(db_path)
    create_tables(conn)
    return conn, conn.cursor()


def create_tables(conn):
    """
    Creates the necessary tables in the database if they do not exist.

    Args:
        conn (sqlite3.Connection): The database connection.
    """
    cursor = conn.cursor()

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS Libros (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        isbn TEXT UNIQUE,
        titulo TEXT,
        autor TEXT,
        genero TEXT
    )
    ''')

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS generos (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        nombre TEXT UNIQUE
    )
    ''')

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS Ingresos (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        libro_id INTEGER,
        fecha_ingreso DATE,
        cantidad INTEGER,
        FOREIGN KEY (libro_id) REFERENCES Libros(id)
    )
    ''')

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS Egresos (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        libro_id INTEGER,
        fecha_egreso DATE,
        cantidad INTEGER,
        FOREIGN KEY (libro_id) REFERENCES Libros(id)
    )
    ''')

    conn.commit()
    cursor.close()
