import tkinter as tk
from tkinter import ttk
from biblioteca.src.crud.add import Add
from biblioteca.src.crud.report import Report
from biblioteca.src.crud.search import Search
from biblioteca.src.utilities.helpers import clear_fields


class MainWindow:
    def __init__(self, root):
        self.root = root
        self.root.title("Gestión de Biblioteca")
        self.root.geometry("950x650")

        self.tab_control = ttk.Notebook(root)

        self.add_tab = ttk.Frame(self.tab_control)
        self.search_tab = ttk.Frame(self.tab_control)
        self.report_tab = ttk.Frame(self.tab_control)

        self.tab_control.add(self.add_tab, text="Alta")
        self.tab_control.add(self.search_tab, text="Consulta")
        self.tab_control.add(self.report_tab, text="Reporte")
        self.tab_control.grid()

        self.add_app = Add(self.add_tab)
        self.search_app = Search(self.search_tab)
        self.report_app = Report(self.report_tab)

        self.tab_control.bind("<<NotebookTabChanged>>", self.on_tab_change)

    def on_tab_change(self, event):
        selected_tab = event.widget.select()
        tab_text = event.widget.tab(selected_tab, "text")

        if tab_text == "Alta":
            self.clear_search_tab()
        elif tab_text == "Consulta":
            clear_fields(self.add_app.isbn_entry, self.add_app.title_entry,
                         self.add_app.author_entry, self.add_app.genre_entry)
        elif tab_text == "Reporte":
            self.report_app.clear_report()

    def clear_search_tab(self):
        self.search_app.title_entry.delete(0, tk.END)
        self.search_app.isbn_entry.delete(0, tk.END)
        self.search_app.genre_entry.set('')

        if self.search_app.modify_button:
            self.search_app.modify_button.destroy()
        if self.search_app.delete_button:
            self.search_app.delete_button.destroy()
        self.search_app.result_treeview.delete(*self.search_app.result_treeview.get_children())
        if self.search_app.frame_form:
            self.search_app.frame_form.destroy()
            self.search_app.frame_form = None


if __name__ == "__main__":
    root = tk.Tk()
    app = MainWindow(root)
    root.mainloop()
