from biblioteca.src.constructor.book import Book


class BookManager:
    @staticmethod
    def find_books_by_criteria(title=None, isbn=None, genre=None):
        """
          Find books based on given criteria such as title, ISBN, or genre.

          Args:
              title (str, optional): The title of the book to search for.
              isbn (str, optional): The ISBN of the book to search for.
              genre (str, optional): The genre of the book to search for.

          Returns:
              tuple: A tuple containing a list of found books and their corresponding IDs.
          """
        books_found = []
        ids = []
        if title or genre:
            if title:
                books_found = Book.find_book_by_title(title)
            if genre:
                books_found = Book.find_by_genre(genre)
            ids = [book.id for book in books_found]
            return books_found, ids
        elif isbn:
            book, an_id = Book.find_book_by_isbn(isbn)
            if book:
                books_found.append(book)
                ids.append(an_id)
                return books_found, ids

    @staticmethod
    def update_book(book_id, title, author, isbn, genre):
        """
        Update the details of a book given its ID.

        Args:
            book_id (int): The ID of the book to update.
            title (str): The new title of the book.
            author (str): The new author of the book.
            isbn (str): The new ISBN of the book.
            genre (str): The new genre of the book.

        Returns:
            bool: True if the book was successfully updated, False otherwise.
        """
        book = Book.find_by_id(book_id)
        if book:
            book.title = title
            book.author = author
            book.isbn = isbn
            book.genre = genre
            try:
                book.save()
                return True
            except ValueError as ve:
                print(f"Error de validación: {ve}")
                raise
            except Exception as e:
                print(f"Error al actualizar el libro: {e}")
                raise
        return False

    @staticmethod
    def delete_book(book_id):
        """
        Delete a book given its ID if it has no transactions.

        Args:
            book_id (int): The ID of the book to delete.

        Returns:
            bool: True if the book was successfully deleted, False otherwise.
        """
        book = Book.find_by_id(book_id)
        if book and not Book.has_transactions(book_id):
            try:
                book.delete(book_id)
                return True
            except Exception as e:
                print(f"Error al eliminar el libro: {e}")
                raise
        return False

    @staticmethod
    def get_book_quantity(book_id):
        """
        Get the quantity of a book given its ID.

        Args:
            book_id (int): The ID of the book.

        Returns:
            int: The quantity of the book.
        """
        return Book.get_book_qty_by_id(book_id)

    @staticmethod
    def find_book_by_isbn(isbn):
        """
        Find a book given its ISBN.

        Args:
            isbn (str): The ISBN of the book to search for.

        Returns:
            Book: The book found with the given ISBN.
        """
        return Book.find_book_by_isbn(isbn)

    @staticmethod
    def find_book_by_id(book_id):
        """
        Find a book given its ID.

        Args:
            book_id (int): The ID of the book to search for.

        Returns:
            Book: The book found with the given ID.
        """
        return Book.find_by_id(book_id)

    @staticmethod
    def has_transactions(book_id):
        """
        Check if a book has any transactions.

        Args:
            book_id (int): The ID of the book to check.

        Returns:
            bool: True if the book has transactions, False otherwise.
        """
        return Book.has_transactions(book_id)
