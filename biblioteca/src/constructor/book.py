import sqlite3
from tkinter import messagebox

from biblioteca.src.utilities.db_utils import get_cursor


class Book:
    def __init__(self, title, author, isbn, genre, book_id=None):
        """
        Initialize a new book instance.

        Args:
            title (str): The title of the book.
            author (str): The author of the book.
            isbn (str): The ISBN of the book.
            genre (str): The genre of the book.
            book_id (int, optional): The ID of the book. Defaults to None.
        """
        self.id = book_id
        self.title = title
        self.author = author
        self.isbn = isbn
        self.genre = genre

    def save(self):
        """
        Save the book to the database. If the book already exists, update its details.

        Raises:
            ValueError: If the ISBN already exists in the database.
            sqlite3.Error: If there is an error saving the book to the database.
        """
        conn, cursor = get_cursor()
        try:
            if self.id:
                cursor.execute('''
                    UPDATE Libros
                    SET titulo = ?, autor = ?, isbn = ?, genero = ?
                    WHERE id = ?
                ''', (self.title, self.author, self.isbn, self.genre, self.id))
            else:
                cursor.execute('''
                    INSERT INTO Libros (titulo, autor, isbn, genero)
                    VALUES (?, ?, ?, ?)
                ''', (self.title, self.author, self.isbn, self.genre))
                self.id = cursor.lastrowid
            conn.commit()
        except sqlite3.IntegrityError:
            raise ValueError("El ISBN ya existe en la base de datos.")
        except sqlite3.Error as e:
            print(f"Error al guardar el libro: {e}")
            raise e
        finally:
            conn.close()

    @staticmethod
    def find_by_id(book_id):
        """
        Find a book by its ID.

        Args:
            book_id (int): The ID of the book to search for.

        Returns:
            Book: The book found with the given ID, or None if not found.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('SELECT * FROM Libros WHERE id = ?', (book_id,))
            row = cursor.fetchone()
            if row:
                return Book(row[1], row[2], row[3], row[4], book_id=row[0])
            return None
        except sqlite3.Error as e:
            print(f"Error al consultar el libro por ID: {e}")
            raise e
        finally:
            conn.close()

    @staticmethod
    def find_all():
        """
        Find all books in the database.

        Returns:
            list: A list of all books.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """

        conn, cursor = get_cursor()
        try:
            cursor.execute('SELECT * FROM Libros')
            rows = cursor.fetchall()
            return [Book(row[1], row[2], row[3], row[4], book_id=row[0]) for row in rows]
        except sqlite3.Error as e:
            print(f"Error al cargar los libros: {e}")
            raise e
        finally:
            conn.close()

    @staticmethod
    def find_book_by_title(partial_title):
        """
        Find books by a partial title match.

        Args:
            partial_title (str): The partial title to search for.

        Returns:
            list: A list of books that match the partial title.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('SELECT * FROM Libros WHERE titulo LIKE ?', ('%' + partial_title + '%',))
            rows = cursor.fetchall()
            return [Book(row[1], row[2], row[3], row[4], book_id=row[0]) for row in rows]
        except sqlite3.Error as e:
            print(f"Error al consultar los libros por título: {e}")
            messagebox.showerror("Error", f"Error al consultar los libros por título: {e}")
        finally:
            conn.close()

    @staticmethod
    def delete(book_id):
        """
        Delete a book from the database given its ID.

        Args:
            book_id (int): The ID of the book to delete.

        Raises:
            sqlite3.Error: If there is an error deleting the book from the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('DELETE FROM Libros WHERE id = ?', (book_id,))
            conn.commit()
        except sqlite3.Error as e:
            print(f"Error al eliminar el libro: {e}")
            raise e
        finally:
            conn.close()

    @staticmethod
    def find_book_by_isbn(isbn):
        """
        Find a book by its ISBN.

        Args:
            isbn (str): The ISBN of the book to search for.

        Returns:
            tuple: A tuple containing the book found and its ID, or (None, None) if not found.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('SELECT * FROM Libros WHERE isbn = ?', (isbn,))
            row = cursor.fetchone()
            if row:
                return Book(row[1], row[2], row[3], row[4], row[0]), row[0]
            return None, None
        except sqlite3.Error as e:
            print(f"Error al consultar el libro por ISBN: {e}")
            messagebox.showerror("Error", f"Error al consultar el libro por ISBN: {e}")
            return None, None
        finally:
            conn.close()

    @staticmethod
    def find_by_genre(genre):
        """
        Find books by their genre.

        Args:
            genre (str): The genre of the books to search for.

        Returns:
            list: A list of books that match the given genre.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('SELECT * FROM Libros WHERE genero = ?', (genre,))
            rows = cursor.fetchall()
            return [Book(row[1], row[2], row[3], row[4], book_id=row[0]) for row in rows]
        except sqlite3.Error as e:
            print(f"Error al consultar los libros por género: {e}")
            raise e
        finally:
            conn.close()

    @staticmethod
    def has_transactions(book_id):
        """
        Check if a book has any transactions.

        Args:
            book_id (int): The ID of the book to check.

        Returns:
            bool: True if the book has transactions, False otherwise.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('''
                SELECT COUNT(*) 
                FROM Ingresos 
                WHERE libro_id = ? 
                UNION 
                SELECT COUNT(*) 
                FROM Egresos 
                WHERE libro_id = ?
            ''', (book_id, book_id))
            total_transactions = sum(row[0] for row in cursor.fetchall())
            return total_transactions > 0
        except sqlite3.Error as e:
            print(f"Error al verificar transacciones del libro: {e}")
            raise e
        finally:
            conn.close()

    @staticmethod
    def get_book_qty_by_id(libro_id):
        """
        Get the quantity of a book given its ID.

        Args:
            libro_id (int): The ID of the book.

        Returns:
            int: The available quantity of the book, or None if not found.

        Raises:
            sqlite3.Error: If there is an error querying the database.
        """
        conn, cursor = get_cursor()
        try:
            cursor.execute('''
                SELECT
                    COALESCE(SUM(i.cantidad), 0) - COALESCE(SUM(e.cantidad), 0) AS cantidad_disponible
                FROM 
                    Libros l
                LEFT JOIN 
                    Ingresos i ON l.id = i.libro_id
                LEFT JOIN 
                    Egresos e ON l.id = e.libro_id
                WHERE 
                    l.id = ?
                GROUP BY 
                    l.id
            ''', (libro_id,))
            row = cursor.fetchone()
            if row:
                return row[0]
            return 0
        except sqlite3.Error as e:
            print(f"Error al consultar la cantidad del libro por ID: {e}")
            messagebox.showerror("Error", f"Error al consultar la cantidad del libro por ID: {e}")
        finally:
            conn.close()
