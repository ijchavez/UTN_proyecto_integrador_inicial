from biblioteca.src.utilities.db_utils import get_cursor


class Category:

    def __init__(self, genre):
        self.genre = genre

    def add_category(self):
        conn, cursor = get_cursor()
        cursor.execute('INSERT INTO generos (nombre) VALUES (?)', (self.genre,))
        conn.commit()
        conn.close()

    @staticmethod
    def search_categories():
        conn, cursor = get_cursor()
        cursor.execute('SELECT genero FROM generos')
        genres = [row[0] for row in cursor.fetchall()]
        conn.close()
        return genres
