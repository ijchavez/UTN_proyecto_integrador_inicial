import tkinter as tk
from tkinter import ttk

from biblioteca.src.constructor.category import Category
from biblioteca.src.constructor.book import Book
from biblioteca.src.utilities.helpers import validate_add_book, validate_isbn, clear_fields
from biblioteca.src.utilities.ui_utils import create_label_entry_pair

categories = Category.search_categories()


def on_validate_isbn(event=None, isbn_entry=None):
    return validate_isbn(isbn_entry)


class Add:
    def __init__(self, root):
        self.isbn_entry = None
        self.title_entry = None
        self.author_entry = None
        self.genre_entry = None
        self.root = root
        self.setup_ui()
    
    def setup_ui(self):
        frame_form = tk.Frame(self.root)
        frame_form.grid(padx=10, pady=10)

        self.isbn_entry = create_label_entry_pair(frame_form, "ISBN:", row=0, column=0)
        self.title_entry = create_label_entry_pair(frame_form, "Título:", row=1, column=0)
        self.author_entry = create_label_entry_pair(frame_form, "Autor:", row=2, column=0)
        self.genre_entry = ttk.Combobox(frame_form, values=categories, state="readonly")
        self.genre_entry = create_label_entry_pair(
            frame_form, "Género:", row=3, column=0, entry_type='combobox', entry_values=categories)

        tk.Button(frame_form, text="Agregar Libro", command=self.add_book).grid(row=4, columnspan=2, pady=10)

    def add_book(self):
        """
            Adds a new book to the library.

            This function collects the book details from the UI entries, validates the ISBN, and if all
            validations pass, it creates a new book record and adds it to the database. After successfully
            adding the book, it clears the input fields.

            Args:
                self (object): The instance of the class containing the method, which provides access to the
                    UI components and methods.

            Returns:
                None

            Raises:
                messagebox.showerror: Displays an error message if the ISBN is invalid or if the book details
                    do not pass validation.
        """
        if not on_validate_isbn(isbn_entry=self.isbn_entry):
            return
        isbn = self.isbn_entry.get()
        title = self.title_entry.get()
        author = self.author_entry.get()
        genre = self.genre_entry.get()

        if validate_add_book(title, author, isbn, genre):
            new_book = Book(title, author, isbn, genre)
            new_book.save()
            clear_fields(self.isbn_entry, self.title_entry, self.author_entry, self.genre_entry)
