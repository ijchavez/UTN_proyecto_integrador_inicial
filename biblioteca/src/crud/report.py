from biblioteca.src.utilities.db_utils import get_cursor
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import ttk


class Report:
    def __init__(self, root):
        self.report_frame = ttk.Frame(root)
        self.report_frame.grid(row=1, column=0, sticky='nsew')
        self.canvas = None

        self.generate_button = ttk.Button(
            self.report_frame, text="Libros por Género", command=self.update_books_by_genre_report)
        self.generate_button.grid(row=1, column=0, padx=10, pady=10)

        self.movements_button = ttk.Button(
            self.report_frame, text="Movimientos Mensuales", command=self.movements_per_month_report)
        self.movements_button.grid(row=2, column=0, padx=10, pady=10)

    def update_books_by_genre_report(self):
        self.clear_report()

        conn, cursor = get_cursor()
        query = """
            SELECT genero, COUNT(*) AS cantidad
            FROM Libros
            GROUP BY genero;
            """
        df = pd.read_sql_query(query, conn)
        conn.close()

        fig, ax = plt.subplots(figsize=(12, 6))
        ax.pie(df['cantidad'], labels=df['genero'], autopct='%1.1f%%', startangle=140)
        ax.set_title('Distribución de Libros por Género')
        ax.axis('equal')

        self.canvas = FigureCanvasTkAgg(fig, master=self.report_frame)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row=3, column=0, sticky="nsew")

    def movements_per_month_report(self):
        self.clear_report()

        conn, cursor = get_cursor()
        query = """
        
            SELECT 
                strftime('%Y-%m', fecha) AS mes,
                SUM(ingresos) AS total_ingresos,
                SUM(egresos) AS total_egresos
            FROM (
                SELECT 
                    fecha, 
                    cantidad AS ingresos, 
                    0 AS egresos
                FROM 
                    Ingresos
                UNION ALL
                SELECT 
                    fecha, 
                    0 AS ingresos, 
                    cantidad AS egresos
                FROM 
                    Egresos
            ) AS movimientos
            GROUP BY 
                mes
            ORDER BY 
                mes;
        """

        df = pd.read_sql_query(query, conn)
        conn.close()
        df['mes'] = pd.to_datetime(df['mes']).dt.strftime('%B %Y')
        df.set_index('mes', inplace=True)

        fig, ax = plt.subplots(figsize=(10, 5))
        df[['total_ingresos', 'total_egresos']].plot(kind='bar', stacked=True, ax=ax)

        ax.set_xlabel('Mes')
        ax.set_ylabel('Cantidad')
        ax.set_title('Movimientos Mensuales de Ingresos y Egresos')
        ax.legend(['Ingresos', 'Egresos'])

        ax.set_xticklabels(df.index, rotation=25, ha='right', fontsize=10)

        self.canvas = FigureCanvasTkAgg(fig, master=self.report_frame)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(row=3, column=0, padx=10, pady=10)

    def clear_report(self):
        if self.canvas:
            self.canvas.get_tk_widget().destroy()
            self.canvas = None
