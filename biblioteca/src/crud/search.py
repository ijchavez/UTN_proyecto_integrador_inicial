import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from biblioteca.src.constructor.book import Book
from biblioteca.src.utilities.helpers import validate_search, validate_isbn
from biblioteca.src.constructor.category import Category
from biblioteca.src.utilities.ui_utils import create_label_entry_pair
from biblioteca.src.manager.book_manager import BookManager

categories = Category.search_categories()


def on_validate_isbn(event=None, isbn_entry=None, current_book_id=None):
    return validate_isbn(isbn_entry, current_book_id)


class Search:
    def __init__(self, root):
        self.root = root
        self.button_frame = None
        self.result_treeview = None
        self.result_frame = None
        self.isbn_entry = None
        self.title_entry = None
        self.genre_entry = None
        self.selected_book_id = None
        self.frame_form = None
        self.delete_button = None
        self.modify_button = None
        self.setup_ui()

    def setup_ui(self):
        frame_form = tk.Frame(self.root)
        frame_form.grid(padx=10, pady=10)

        self.isbn_entry = create_label_entry_pair(frame_form, "Buscar por ISBN:", row=0, column=0)
        self.title_entry = create_label_entry_pair(frame_form, "Buscar por Título:", row=1, column=0)
        self.genre_entry = create_label_entry_pair(
            frame_form, "Buscar por Genero:", row=2, column=0, entry_type='combobox', entry_values=categories)

        clean_genre_button = tk.Button(frame_form, text="Limpiar Genero", command=self.clean_genre)
        clean_genre_button.grid(row=2, column=4)

        search_button = tk.Button(frame_form, text="Buscar Libro", command=self.search_book)
        search_button.grid(row=4, columnspan=2, pady=10)

        self.result_frame = tk.Frame(self.root)
        self.result_frame.grid(row=3, column=0, padx=10, pady=10, sticky="nsew")

        columns = ("ISBN", "Titulo", "Autor", "Genero", "Cantidad")
        self.result_treeview = ttk.Treeview(self.result_frame, columns=columns, show="headings")
        for col in columns:
            self.result_treeview.heading(col, text=col)
            self.result_treeview.column(col, minwidth=0, width=140, stretch=tk.NO)
        self.result_treeview.grid(row=0, column=0, sticky="nsew")

        self.result_treeview.bind("<ButtonRelease-1>", self.on_row_selected)

        self.result_frame.grid_rowconfigure(0, weight=1)
        self.result_frame.grid_columnconfigure(0, weight=1)

        self.button_frame = tk.Frame(self.result_frame)
        self.button_frame.grid(row=0, column=1, sticky="ns")

    def search_book(self):
        self.update_view_after_deletion()

        title = self.title_entry.get().strip()
        isbn = self.isbn_entry.get().strip()
        genre = self.genre_entry.get().strip()

        if not validate_search(self, title, isbn, genre):
            return

        books_found, ids = BookManager.find_books_by_criteria(title, isbn, genre)
        quantity_available = []

        for book in books_found:
            quantity = BookManager.get_book_quantity(book.id)
            if quantity is None:
                quantity = 0
            quantity_available.append(quantity)

        self.result_treeview.delete(*self.result_treeview.get_children())
        if books_found:
            for i, book in enumerate(books_found):
                self.result_treeview.insert("", "end", values=(
                    book.isbn, book.title, book.author, book.genre, quantity_available[i]
                ))
        else:
            messagebox.showinfo("Oops", "Libro no encontrado")

    def on_row_selected(self, event):
        selected_item = self.result_treeview.selection()
        if selected_item:
            selected_book = self.result_treeview.item(selected_item)
            book_details = selected_book['values']

            book, self.selected_book_id = BookManager.find_book_by_isbn(book_details[0])

            if not self.delete_button:
                self.delete_button = tk.Button(
                    self.button_frame, text="Eliminar", command=self.delete_selected_book, width=10, height=1
                )
                self.delete_button.grid(row=0, column=0, pady=2, padx=2)

            if not self.modify_button:
                self.modify_button = tk.Button(
                    self.button_frame, text="Modificar", command=self.modify_selected_book, width=10, height=1
                )
                self.modify_button.grid(row=1, column=0, pady=2, padx=2)

    def delete_selected_book(self):
        if self.selected_book_id is not None:
            selected_book = BookManager.find_book_by_id(self.selected_book_id)
            if BookManager.has_transactions(self.selected_book_id):
                messagebox.showwarning("Eliminar libro", f"El libro '{selected_book.title}' no se puede eliminar "
                                                         f"porque tiene transacciones asociadas.")
                return
            if messagebox.askyesno(
                    "Eliminar libro", f"¿Estás seguro de que deseas eliminar el libro '{selected_book.title}'?"):
                BookManager.delete_book(self.selected_book_id)
                # selected_book.delete_book(self.selected_book_id) BUG
                messagebox.showinfo("Eliminar libro", "Libro eliminado correctamente")
                self.update_view_after_deletion()

    def modify_selected_book(self):
        if self.selected_book_id is not None:
            book = BookManager.find_book_by_id(self.selected_book_id)

            def destroy_form():
                if self.frame_form and self.frame_form.winfo_exists():
                    self.frame_form.destroy()

            destroy_form()

            self.frame_form = tk.Frame(self.root)
            self.frame_form.grid(padx=10, pady=10)

            isbn_entry = create_label_entry_pair(self.frame_form, "ISBN:", row=0, column=0)
            isbn_entry.insert(0, book.isbn)

            title_entry = create_label_entry_pair(self.frame_form, "Título:", row=1, column=0)
            title_entry.insert(0, book.title)

            author_entry = create_label_entry_pair(self.frame_form, "Autor:", row=2, column=0)
            author_entry.delete(0, tk.END)
            author_entry.insert(0, book.author)

            genre_entry = create_label_entry_pair(
                self.frame_form, "Género:", row=3, column=0, entry_type="combobox", entry_values=categories)
            genre_entry.set(book.genre)

            def save_changes():
                if not on_validate_isbn(isbn_entry=isbn_entry, current_book_id=self.selected_book_id):
                    return

                updated_book = Book(
                    title=title_entry.get().strip(),
                    author=author_entry.get().strip(),
                    isbn=isbn_entry.get().strip(),
                    genre=genre_entry.get().strip()
                )

                if updated_book.title and updated_book.author and updated_book.isbn and updated_book.genre:
                    try:
                        BookManager.update_book(self.selected_book_id, updated_book.title,
                                                updated_book.author, updated_book.isbn, updated_book.genre)
                        messagebox.showinfo("Modificación", "Libro modificado correctamente")
                        self.update_view_after_deletion()
                        self.title_entry.delete(0, tk.END)
                        self.isbn_entry.delete(0, tk.END)
                        self.genre_entry.set('')
                    except Exception as e:
                        messagebox.showerror("Error al modificar el libro", str(e))
                else:
                    messagebox.showerror("Error", "Todos los campos son obligatorios.")

            save_button = tk.Button(self.frame_form, text="Guardar cambios", command=save_changes)
            save_button.grid(row=4, columnspan=2, pady=10)

    def update_view_after_deletion(self):
        self.result_treeview.delete(*self.result_treeview.get_children())
        if self.delete_button:
            self.delete_button.destroy()
            self.delete_button = None
        if self.modify_button:
            self.modify_button.destroy()
            self.modify_button = None
        if self.frame_form:
            self.frame_form.destroy()
            self.frame_form = None

    def clean_genre(self):
        self.genre_entry.set('')

